import { IpfsService, service } from "space-mvc";
import fetch from 'node-fetch'

// @service()
// class DownloadService {

//     constructor(
//         private ipfsService:IpfsService
//     ) {}

    
//     async downloadAll() {

//         for (let i=1; i < 22795; i++) {

//             try {
//                 let data = await this.downloadImage(i)
//                 let cid = await this.saveImage(i, data)
//             } catch(ex) {
//                 console.log("THE END")
//                 break
//             }

//         }

//     }

//     async downloadImage(id:number) : Promise<Uint8Array> {

//         console.log(`Downloading image for ${id}`)

//         let response =  await fetch(`https://d35vxokfjoq7rk.cloudfront.net/0x8c9b261faef3b3c2e64ab5e58e04615f8c788099/${id}.png?d=847`)
//         let buffer = response.buffer()

//         return Uint8Array.from(buffer)

//     }
    
//     async saveImage(id:number, data:Uint8Array) : Promise<CID> {

//         let path = `/mlbc/${id}`

//         await this.ipfsService.ipfs.files.write(path, data, {
//             create: true,
//             parents: true
//         })

//         return this.getImageCid(id)

//     }

//     async getImageCid(id:number) : Promise<CID> {

//         let stat = await this.ipfsService.ipfs.files.stat(`/mlbc/${id}`)

//         return stat.cid

//     }

// }

// export {
//     DownloadService
// }