import { MetastoreService, Initializable } from "space-mvc";
import { Student } from "../dto/student";
declare class StudentDao implements Initializable {
    private metastoreService;
    private store;
    constructor(metastoreService: MetastoreService);
    init(): Promise<void>;
    put(key: string, student: Student): Promise<any>;
    get(key: string): Promise<Student>;
    list(offset?: number, limit?: number): Promise<Student[]>;
}
export { StudentDao };
