# The NFT Commons
Remix and meme friendly NFTs. Easily create Etheruem NFTs with friendly Creative Commons licensing that encourages sharing, remixing, and memes. 

The NFT Commons is an alternative way to monetize creative work while contributing the work itself to the digital collective. Think of them as a way to keep track of who's the biggest fan of the piece just like any other art market. 

* Do art because you love it 
* Put it in the public domain 
* Get paid anyway 

* Digitally sign your art.
* Publish offline and make available to peers.
* Anyone can buy an NFT print of your media. You set the price.
* NFT prints can be exchanged on third-party marketplaces like OpenSea.
* The digital collective gets more art and whoever appreciates it the most can buy a special souvenier.
* Reward early fans of artists.
* It's nice to own stuff. Relative scarcity creates a real sense of ownership. Who's the biggest fan?
* Earn tokens for holding. The longer you hold the more tokens you get. Tokens are just for fun.
* Free, open source, and peer to peer.

How it works
You host your content in the browser. You always have a copy. Automatically share with peers.


The NFT Commons does not have a governance token. Everyone has a say in the commons, even those who don't have tokens yet. Fork the git repo and create your own customized version.





## Build

Development build. Creates a web server, builds, and watches for changes. 

```console
npm run start:dev 
```

Distribution build. Webpack populates the public folder with a compilied and minimized copy of the progressive web app. 

```console
npm run build
```

